# Execute Discord Webhook
## Description
This repo is a template for sending a Discord message via a webhook by executing a pipeline job.

## Usage

1. Fork this repo.
2. Set the pipeline variable `WEBHOOK_URL`.
3. Update `body.json` or create a new json file according to the [Discord developer documentation](https://discord.com/developers/docs/resources/webhook#execute-webhook).
4. Call the pipeline with the `CONTENT` variable pointing to the `.json` containing the message.

## License
Do whatever you want with this. I am not responsible for anything if you manage to break something or you get banned by Gitlab or Discord. 

